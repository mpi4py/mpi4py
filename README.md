MPI for Python
==============

**WARNING**: This repository has been archived.

**WARNING**: All mpi4py development has moved to [GitHub](https://github.com/mpi4py/mpi4py).

This repository clone at Bitbucket contains mpi4py commits up to **Aug 14, 2021**.

To update your local repository to use the new location, execute

```
git remote set-url origin git@github.com:mpi4py/mpi4py.git
```

or

```
git remote set-url origin https://github.com/mpi4py/mpi4py.git
```
